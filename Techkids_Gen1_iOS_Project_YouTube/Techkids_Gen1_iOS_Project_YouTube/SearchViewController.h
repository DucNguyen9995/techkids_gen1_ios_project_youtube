//
//  SearchTableViewController.h
//  Techkids_Gen1_iOS_Project_YouTube
//
//  Created by Duc Nguyen on 1/17/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController <UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) IBOutlet BaseTableView *tableView;
@property(nonatomic, strong) IBOutlet UISearchBar *searchBar;

@end

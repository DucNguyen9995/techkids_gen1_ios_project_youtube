//
//  Constants.h
//  Techkids_Gen1_iOS_Project_YouTube
//
//  Created by Duc Nguyen on 1/14/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#pragma mark - Keys
#define kAPIKey     @"AIzaSyCiHDVLYaeOTdbYH6ZhZuEMz5sOBR2bK9k"

#pragma mark - Links
#define kSuggestAPILink     @"https://suggestqueries.google.com/complete/search?client=youtube&ds=yt&client=firefox&q="

#pragma mark - YouTube Query Part
#define kYouTubeQueryPartSnippet            @"snippet"
#define kYouTubeQueryPartIdentifier         @"id"
#define kYouTubeQueryPartContentDetails     @"contentDetails"
#define kYouTubeQueryPartStatistics         @"statistics"
#define kYouTubeQueryChartMostPopular       @"mostPopular"

#pragma mark - Table View Cell Identifiers
#define kTableViewCellNormal                @"Cell"
#define kTableViewCellVideo                 @"VideoCell"
#define kTableViewCellCategory              @"CategoryTableViewCell"

#pragma mark - Table View Cell Height
#define kTableViewCellHeightVideo           76.0f
#define kTableViewCellHeightCategory        76.0f
#define kTableViewCellHeightSuggest         35.0f

#endif /* Constants_h */

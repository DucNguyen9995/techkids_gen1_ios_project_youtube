//
//  ViewController+PlayStatus.m
//  Techkids_Gen1_iOS_Project_YouTube
//
//  Created by Duc Nguyen on 1/22/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "ViewController+PlayStatus.h"

@interface UIViewController ()

@end

@implementation UIViewController (PlayStatus)
@dynamic playing;

@end

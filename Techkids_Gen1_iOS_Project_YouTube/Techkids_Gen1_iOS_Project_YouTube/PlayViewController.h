//
//  PlayViewController.h
//  Techkids_Gen1_iOS_Project_YouTube
//
//  Created by Duc Nguyen on 1/23/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayViewController : UIViewController <UITableViewDelegate>

@property (nonatomic, strong) GTLYouTubeVideo *video;

#pragma mark - Upper View

@property (weak, nonatomic) IBOutlet UIButton *btnDismiss;
@property (weak, nonatomic) IBOutlet UIView *viewPlayVideo;
@property (weak, nonatomic) IBOutlet UIImageView *viewThumbnail;

#pragma mark - Video Details & Suggest Videos

@property (weak, nonatomic) IBOutlet UILabel *lblVideoTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblNumbersOfView;
@property (weak, nonatomic) IBOutlet BaseTableView *tblSuggestVideo;

- (IBAction)btnDismissDidTap:(id)sender;

@end

//
//  ViewController+PlayStatus.h
//  Techkids_Gen1_iOS_Project_YouTube
//
//  Created by Duc Nguyen on 1/22/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (PlayStatus)

@property(nonatomic, assign) BOOL playing;

@end

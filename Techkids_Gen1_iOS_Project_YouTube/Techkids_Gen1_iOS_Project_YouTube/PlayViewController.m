//
//  PlayViewController.m
//  Techkids_Gen1_iOS_Project_YouTube
//
//  Created by Duc Nguyen on 1/23/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "PlayViewController.h"
#import "MPMoviePlayerController+BackgroundPlayback.h"

@interface PlayViewController ()

@property(nonatomic, strong) XCDYouTubeVideoPlayerViewController *videoPlayerViewController;

@end

@implementation PlayViewController
@synthesize video;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadVideoThumbnail];
    [self.btnDismiss setImage:[UIImage imageNamed:@"arrow"] forState:UIControlStateNormal | UIControlStateHighlighted];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma mark - Fetch Suggest List

- (void)fetchSuggestListWithVideoIdentifier:(NSString *)videoId {
    GTLServiceYouTube *service = self.tblSuggestVideo.youTubeService;
    
    GTLQueryYouTube *query = [GTLQueryYouTube queryForSearchListWithPart:kYouTubeQueryPartSnippet];
    query.relatedToVideoId = videoId;
    query.type = @"video";
    query.maxResults = 20;
    
    [service executeQuery:query
        completionHandler:^(GTLServiceTicket *ticket, GTLYouTubeSearchListResponse *searchListResponse, NSError *error) {
            [service executeQuery:query
                completionHandler:^(GTLServiceTicket *ticket, GTLYouTubeSearchListResponse *searchListResponse, NSError *error) {
                    if (error) {
                        GTLErrorObject * const errorObject = error.userInfo[kGTLStructuredErrorKey];
                        NSLog(@"error from YouTube API: %@", errorObject.data);
                    } else {
                        NSDictionary *jsonDict = [searchListResponse JSON];
                        NSArray *listItems = [jsonDict objectForKey:@"items"];
                        NSMutableArray *listID = [[NSMutableArray alloc] init];
                        for (NSDictionary *dictObject in listItems) {
                            NSString *videoID = dictObject[@"id"][@"videoId"];
                            [listID addObject:videoID];
                        }
                        NSString *stringIDs = [listID componentsJoinedByString:@","];
                        [self fetchVideoListFromSearchList:stringIDs];
                    }
                }];
        }];
    
}

- (void)fetchVideoListFromSearchList:(NSString *)stringID {
    GTLServiceYouTube *service = self.tblSuggestVideo.youTubeService;
    
    NSArray *arrQueryParts = @[kYouTubeQueryPartSnippet,kYouTubeQueryPartContentDetails,kYouTubeQueryPartStatistics];
    NSString *strQueryParts = [arrQueryParts componentsJoinedByString:@","];
    GTLQueryYouTube *query = [GTLQueryYouTube queryForVideosListWithPart:strQueryParts];
    query.identifier = stringID;
    
    [service executeQuery:query
        completionHandler:^(GTLServiceTicket *ticket, GTLYouTubeVideoListResponse *videoListResponse, NSError *error) {
            if (!error) {
                // Success
                self.tblSuggestVideo.list = [videoListResponse items];
                NSLog(@"%ld",[[videoListResponse items] count]);
                [self.tblSuggestVideo reloadData];
            } else {
                // Fail
                GTLErrorObject *errorObject = error.userInfo[kGTLStructuredErrorKey];
                NSLog(@"error from YouTube API: %@",errorObject.data);
            }
        }];
}


#pragma mark - Load Thumbnail Before Play Video

- (void)loadVideoThumbnail {
    NSString *videoIdentifier = [video identifier];
    self.videoPlayerViewController = [[XCDYouTubeVideoPlayerViewController alloc] initWithVideoIdentifier:videoIdentifier];
    
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self selector:@selector(videoPlayerViewControllerDidReceiveVideo:) name:XCDYouTubeVideoPlayerViewControllerDidReceiveVideoNotification object:self.videoPlayerViewController];
    [defaultCenter addObserver:self selector:@selector(moviePlayerPlaybackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:self.videoPlayerViewController.moviePlayer];
}

#pragma mark - Play Video And Handle The Stop

- (void)play {
    [self.videoPlayerViewController presentInView:self.viewPlayVideo];
    [self.videoPlayerViewController.moviePlayer play];
}

- (void)videoPlayerViewControllerDidReceiveVideo:(NSNotification *)notification
{
    self.lblVideoTitle.text = video.snippet.title;
    
    NSURL *thumbnailURL = [NSURL URLWithString:(video.snippet.thumbnails.medium.url ?: video.snippet.thumbnails.defaultProperty.url)];
    [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:thumbnailURL] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         self.viewThumbnail.image = [UIImage imageWithData:data];
         [self play];
     }];
}

- (void)moviePlayerPlaybackDidFinish:(NSNotification *)notification
{
    NSError *error = notification.userInfo[XCDMoviePlayerPlaybackDidFinishErrorUserInfoKey];
    if (error)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:error.localizedDescription delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)reloadWhenClickedSuggestionInViewWithYouTubeVideo:(GTLYouTubeVideo *)youtubeVideo {
    video = youtubeVideo;
    [self loadVideoThumbnail];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    GTLYouTubeVideo *videoToReload = [self.tblSuggestVideo.list objectAtIndex:indexPath.row];
    [self reloadWhenClickedSuggestionInViewWithYouTubeVideo:videoToReload];
    
    NSArray *arr = @[indexPath];
    [tableView reloadRowsAtIndexPaths:arr
                     withRowAnimation:UITableViewRowAnimationAutomatic];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnDismissDidTap:(id)sender {
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 
                             }];
}

@end

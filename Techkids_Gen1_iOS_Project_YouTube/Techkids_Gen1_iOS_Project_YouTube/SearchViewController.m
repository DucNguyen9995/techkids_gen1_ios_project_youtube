//
//  SearchTableViewController.m
//  Techkids_Gen1_iOS_Project_YouTube
//
//  Created by Duc Nguyen on 1/17/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "SearchViewController.h"

@interface SearchViewController ()

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem.leftBarButtonItem backButtonBackgroundImageForState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    if (![self.searchBar isFirstResponder]) {
        [self.searchBar setShowsCancelButton:NO animated:YES];
    } else {
        [self.searchBar setShowsCancelButton:YES animated:YES];
    }
    
    [self setTitle:@"Search"];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Search bar delegate and search things

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    self.tableView.listAlternate = self.tableView.list;
    [searchBar setShowsCancelButton:YES animated:YES];
    if (![searchBar.text isEqualToString:@""]) {
        [self fetchSearchSuggestsByKeyWord:searchBar.text];
    }
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self fetchSearchSuggestsByKeyWord:searchText];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    self.tableView.list = self.tableView.listAlternate;
    self.tableView.listAlternate = nil;
    [searchBar resignFirstResponder];
    [self searchWithKeyWord:self.searchBar.text
              withMaxResult:20];
    [searchBar setShowsCancelButton:NO
                           animated:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    self.tableView.list = self.tableView.listAlternate;
    [searchBar setShowsCancelButton:NO
                           animated:YES];
}

- (void)fetchSearchSuggestsByKeyWord:(NSString *)keyWord {
    NSString *stringURL = [NSString stringWithFormat:@"%@%@", kSuggestAPILink, keyWord];
    stringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:stringURL];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:url.absoluteString
      parameters:nil
        progress:nil
         success:^(NSURLSessionDataTask * _Nonnull task, NSArray *responseObject) {
             // Success. So set responseObject in self.list
             self.tableView.list = [responseObject objectAtIndex:1];
             [self.tableView reloadData];
         } failure:nil];
}

- (void)searchWithKeyWord:(NSString *)keyWord withMaxResult:(NSUInteger)maxResult{
    GTLServiceYouTube *service = self.tableView.youTubeService;
    
    NSArray *arrQueryParts = @[kYouTubeQueryPartSnippet,kYouTubeQueryPartIdentifier];
    NSString *strQueryParts = [arrQueryParts componentsJoinedByString:@","];
    GTLQueryYouTube *query = [GTLQueryYouTube queryForSearchListWithPart:strQueryParts];
    query.q = keyWord;
    query.maxResults = maxResult;
    
    [service executeQuery:query
        completionHandler:^(GTLServiceTicket *ticket, GTLYouTubeSearchListResponse *searchListResponse, NSError *error) {
            if (error) {
                GTLErrorObject * const errorObject = error.userInfo[kGTLStructuredErrorKey];
                NSLog(@"error from YouTube API: %@", errorObject.data);
            } else {
                NSDictionary *jsonDict = [searchListResponse JSON];
                NSArray *listItems = [jsonDict objectForKey:@"items"];
                NSMutableArray *listID = [[NSMutableArray alloc] init];
                for (NSDictionary *dictObject in listItems) {
                    NSString *videoID = dictObject[@"id"][@"videoId"];
                    NSLog(@"videoID: %@",videoID);
                    if (videoID == nil) {
                        continue;
                    }
                    [listID addObject:videoID];
                }
                NSString *stringIDs = [listID componentsJoinedByString:@","];
                [self fetchVideoListFromSearchList:stringIDs];
            }
        }];
}

- (void)fetchVideoListFromSearchList:(NSString *)stringID {
    GTLServiceYouTube *service = self.tableView.youTubeService;
    
    NSArray *arrQueryParts = @[kYouTubeQueryPartSnippet,kYouTubeQueryPartContentDetails,kYouTubeQueryPartStatistics];
    NSString *strQueryParts = [arrQueryParts componentsJoinedByString:@","];
    GTLQueryYouTube *query = [GTLQueryYouTube queryForVideosListWithPart:strQueryParts];
    query.identifier = stringID;
    
    [service executeQuery:query
        completionHandler:^(GTLServiceTicket *ticket, GTLYouTubeVideoListResponse *videoListResponse, NSError *error) {
            if (!error) {
                // Success
                self.tableView.list = [videoListResponse items];
                NSLog(@"%ld",[[videoListResponse items] count]);
                [self.tableView reloadData];
            } else {
                // Fail
                GTLErrorObject *errorObject = error.userInfo[kGTLStructuredErrorKey];
                NSLog(@"error from YouTube API: %@",errorObject.data);
            }
        }];
}

#pragma mark - Table View Data Source

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.searchBar isFirstResponder]) {
        [self searchWithKeyWord:[[[tableView cellForRowAtIndexPath:indexPath] textLabel] text] withMaxResult:20];
        [self.searchBar resignFirstResponder];
        return;
    }
    
    PlayViewController *playViewController = [[PlayViewController alloc] init];
    NSLog(@"indexPath.row = %ld",indexPath.row);
    playViewController.video = (GTLYouTubeVideo *)[self.tableView.list objectAtIndex:indexPath.row];
    [self presentViewController:playViewController
                       animated:YES
                     completion:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.tableView numberOfSections];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.tableView numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self.tableView cellForRowAtIndexPath:indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.searchBar isFirstResponder]) {
        return kTableViewCellHeightSuggest;
    }
    return kTableViewCellHeightVideo;
}
@end

//
//  CustomReuseableCellTableViewCell.m
//  YoutubeParser
//
//  Created by Duc Nguyen on 1/7/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "VideoTableViewCell.h"

@implementation VideoTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  CommonUseMethods.h
//  Techkids_Gen1_iOS_Project_YouTube
//
//  Created by Duc Nguyen on 1/14/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface  CommonUseMethods : NSObject

+ (NSString*) parseDuration:(NSString*) duration;
+ (NSString*) suffixNumber:(NSNumber*)number;
+ (NSString*)formatNumberOfView:(NSString*)number;

@end

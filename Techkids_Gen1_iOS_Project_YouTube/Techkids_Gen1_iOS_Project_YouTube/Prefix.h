//
//  Prefix.h
//  Techkids_Gen1_iOS_Project_YouTube
//
//  Created by Duc Nguyen on 1/14/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import <Availability.h>

#ifndef __IPHONE_4_0
#warning "This project uses features only available in iOS SDK 4.0 and later."
#endif

#ifdef __OBJC__

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#pragma mark - Network Module & YouTube API
#import "AFNetworking.h"
#import "SDWebImage.h"
#import "GTLYouTube.h"

#pragma mark - View Controller Stuffs
#import "BaseTableView.h"
#import "PlayViewController.h"

#pragma mark - Constants Area
#import "Constants.h"

#pragma mark - Common Use File
#import "CommonUseMethods.h"

#pragma mark - Play Video
#import "XCDYouTubeKit.h"

#endif
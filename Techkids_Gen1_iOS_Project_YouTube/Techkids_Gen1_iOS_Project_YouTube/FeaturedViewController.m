//
//  ViewController.m
//  Techkids_Gen1_iOS_Project_YouTube
//
//  Created by Duc Nguyen on 1/14/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "FeaturedViewController.h"

@interface FeaturedViewController ()

@property(nonatomic, strong) NSMutableDictionary *dictVideoList;

@end

@implementation FeaturedViewController

- (void)viewDidLoad {
    [self fetchVideoCategories];
    self.title = @"Featured";
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Dispatch Once

- (NSMutableDictionary *)dictVideoList {
    static NSMutableDictionary *dict;
    
    static dispatch_once_t dispatchOnceToken;
    dispatch_once(&dispatchOnceToken, ^{
        dict = [[NSMutableDictionary alloc] init];
    });
    
    return dict;
}

#pragma mark - Fetch Things

- (void)fetchVideoListWithVideoCategoryIdentifier:(NSString *)videoCategoryIdentifier {
    GTLServiceYouTube *service = self.tableView.youTubeService;
    
    NSArray *arrTemp = @[kYouTubeQueryPartSnippet,kYouTubeQueryPartContentDetails,kYouTubeQueryPartStatistics];
    NSString *queryPart = [arrTemp componentsJoinedByString:@","];
    
    GTLQueryYouTube *query = [GTLQueryYouTube queryForVideosListWithPart:queryPart];
    [query setVideoCategoryId:videoCategoryIdentifier];
    [query setMaxResults:20];
//    [query setRegionCode:[[NSLocale currentLocale] objectForKey:NSLocaleCountryCode]];
    [query setChart:@"mostPopular"];
    
    [service executeQuery:query
        completionHandler:^(GTLServiceTicket *ticket, GTLYouTubeVideoListResponse *videoList, NSError *error) {
            if (!error) {
                // Success
                [self.tableView.dictCategoriesDetails setObject:videoList forKey:videoCategoryIdentifier];
                NSLog(@"%@",self.tableView.dictCategoriesDetails);
                [self.tableView reloadData];
            } else {
                // Fail
            }
        }];
}

- (void)fetchVideoCategories {
    GTLServiceYouTube *service = self.tableView.youTubeService;
    
    GTLQueryYouTube *query = [GTLQueryYouTube queryForVideoCategoriesListWithPart:kYouTubeQueryPartSnippet];
    query.regionCode = [[NSLocale currentLocale] objectForKey:NSLocaleCountryCode];
    
    [service executeQuery:query
        completionHandler:^(GTLServiceTicket *ticket, GTLYouTubeVideoCategoryListResponse *videoCategoryListResponse, NSError *error) {
            if (!error) {
                // Success
                self.tableView.list = [videoCategoryListResponse items];
                for (GTLYouTubeVideoCategory *videoCategory in videoCategoryListResponse) {
                    [self fetchVideoListWithVideoCategoryIdentifier: [videoCategory identifier]];
                    [self.tableView reloadData];
                }
            } else {
                // Fail
            }
        }];
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.tableView numberOfSections];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.tableView numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self.tableView cellForRowAtIndexPath:indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat cellHeight = kTableViewCellHeightCategory;
    return cellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Selected and what? Write Here
    
    NSArray *arrIndexPath = @[indexPath];
    [tableView reloadRowsAtIndexPaths:arrIndexPath
                     withRowAnimation:UITableViewRowAnimationAutomatic];
    
}

@end

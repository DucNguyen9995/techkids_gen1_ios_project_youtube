//
//  ReuseTableView.m
//  YoutubeParser
//
//  Created by Duc Nguyen on 1/10/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "BaseTableView.h"
#import "VideoTableViewCell.h"
#import "CategoryTableViewCell.h"

@interface BaseTableView ()

@end


@implementation BaseTableView

#pragma mark - Synthesize properties
// If list is an Array of video category then dict have the first video of Video's Category
// If list is an Array of video then dict is nil
// If list is an Array of suggestions then dict is nil
@synthesize list;
@synthesize dictCategoriesDetails;
@synthesize listAlternate;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

#pragma mark - Dispatch Once

- (NSArray *)list {
    static NSArray *arr;
    
    static dispatch_once_t dispatchOnceToken;
    dispatch_once(&dispatchOnceToken, ^{
        arr = [[NSArray alloc] init];
    });
    
    return arr;
}

- (NSArray *)listAlternate {
    static NSArray *arr;
    
    static dispatch_once_t dispatchOnceToken;
    dispatch_once(&dispatchOnceToken, ^{
        arr = [[NSArray alloc] init];
    });
    
    return arr;
}

- (GTLServiceYouTube *)youTubeService {
    static GTLServiceYouTube *service;
    
    static dispatch_once_t dispatchOnceToken;
    dispatch_once(&dispatchOnceToken, ^{
        service = [[GTLServiceYouTube alloc]init];
        
        [service setAPIKey:kAPIKey];
        
        service.retryEnabled = YES;
        
    });
    
    return service;
}

- (NSMutableDictionary *)dictCategoriesDetails {
    static NSMutableDictionary *dict;
    
    static dispatch_once_t dispatchOnceToken;
    dispatch_once(&dispatchOnceToken, ^{
        dict = [[NSMutableDictionary alloc] init];
    });
    
    return dict;
}

#pragma mark - Table Datasource

- (NSInteger)numberOfSections {
    self.separatorStyle = UITableViewCellSeparatorStyleNone;
    return 1;
}

- (NSInteger)numberOfRowsInSection:(NSInteger)section {
    NSLog(@"%ld",list.count);
    return [list count];
}

- (UITableViewCell *)cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([[list objectAtIndex:0] isKindOfClass:[GTLYouTubeVideoCategory class]]) {
        if (dictCategoriesDetails != nil) {
            [self registerClass:[CategoryTableViewCell class] forCellReuseIdentifier:kTableViewCellCategory];
            [self registerNib:[UINib nibWithNibName:@"CategoryTableViewCell"
                                             bundle:nil] forCellReuseIdentifier:kTableViewCellCategory];
            
            CategoryTableViewCell *cell = [self dequeueReusableCellWithIdentifier:kTableViewCellCategory forIndexPath:indexPath];
            if (cell == nil) {
                cell = [[CategoryTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kTableViewCellCategory];
            }
            NSString *videoCategoryTitle = [[[list objectAtIndex:indexPath.row] snippet] title];
            cell.lblCategoryName.text = videoCategoryTitle;
            
                NSURL *url = [[NSURL alloc] initWithString:[[[[dictCategoriesDetails[[[list objectAtIndex:indexPath.row] identifier]] snippet] thumbnails] defaultProperty] url]];
                [cell.imgView sd_setImageWithURL:url
                                placeholderImage:nil
                                         options:SDWebImageRefreshCached
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                           [cell setNeedsLayout];
                                       }];
            return cell;
        } else {
            UITableViewCell *cell = [self dequeueReusableCellWithIdentifier:kTableViewCellNormal
                                                                    forIndexPath:indexPath];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                              reuseIdentifier:kTableViewCellNormal];
            }
            NSString *videoCategoryTitle = [[[list objectAtIndex:indexPath.row] snippet] title];
            cell.textLabel.text = videoCategoryTitle;
            
            return cell;
        }
    } else if ([[list objectAtIndex:0] isKindOfClass:[GTLYouTubeVideo class]]){
        [self registerClass:[VideoTableViewCell class] forCellReuseIdentifier:kTableViewCellVideo];
        [self registerNib:[UINib nibWithNibName:@"VideoTableViewCell"
                                         bundle:nil] forCellReuseIdentifier:kTableViewCellVideo];
        VideoTableViewCell *cell = [self dequeueReusableCellWithIdentifier:kTableViewCellVideo];
        if (cell == nil) {
            cell = [[VideoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                             reuseIdentifier:kTableViewCellVideo];
        }
        
        // Configure cells
        // Get video at row
        GTLYouTubeVideo* video = [list objectAtIndex:indexPath.row];
        
        // Cell title
        NSString *videoTitle = [[video snippet] title];
        cell.lblVideoName.text = videoTitle;
        
        // Cell channel
        NSString *videoChannel = [[video snippet] channelTitle];
        cell.lblChannel.text = videoChannel;
        
        // Cell Number of views
        NSString *videoViews = [CommonUseMethods formatNumberOfView:[NSString stringWithFormat:@"%@",[[video statistics] viewCount]]];
        cell.lblNumberOfViews.text = videoViews;
        
        // Cell Duration
        NSString *videoDuration = [CommonUseMethods parseDuration:[[video contentDetails] duration]];
        cell.lblDuration.text = videoDuration;
        
        // Cell Image
        NSURL *url = [NSURL URLWithString:[[[[[list objectAtIndex:indexPath.row] snippet] thumbnails] defaultProperty] url]];
        [cell.imgView sd_setImageWithURL:url
                        placeholderImage:nil
                                 options:SDWebImageRefreshCached
                               completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                   [cell setNeedsLayout];
                               }];
        
        
        return cell;
    } else {
        UITableViewCell *cell = [self dequeueReusableCellWithIdentifier:kTableViewCellNormal forIndexPath:indexPath];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:kTableViewCellNormal];
        }
        
        cell.textLabel.text = [list objectAtIndex:indexPath.row];
        
        return cell;
    }
}

@end

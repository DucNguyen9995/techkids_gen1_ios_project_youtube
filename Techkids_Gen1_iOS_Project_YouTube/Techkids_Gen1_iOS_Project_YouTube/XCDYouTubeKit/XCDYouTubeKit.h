//
//  Copyright (c) 2013-2015 Cédric Luthi. All rights reserved.
//

#import <TargetConditionals.h>

#import "XCDYouTubeClient.h"
#import "XCDYouTubeError.h"
#import "XCDYouTubeOperation.h"
#import "XCDYouTubeVideo.h"
#import "XCDYouTubeVideoOperation.h"

#if TARGET_OS_IOS || (!defined(TARGET_OS_IOS) && TARGET_OS_IPHONE)
#import "XCDYouTubeVideoPlayerViewController.h"
#endif

//
//  main.m
//  Techkids_Gen1_iOS_Project_YouTube
//
//  Created by Duc Nguyen on 1/14/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

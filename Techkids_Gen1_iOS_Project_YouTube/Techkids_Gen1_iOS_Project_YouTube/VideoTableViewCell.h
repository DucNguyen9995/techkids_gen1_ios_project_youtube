//
//  CustomReuseableCellTableViewCell.h
//  YoutubeParser
//
//  Created by Duc Nguyen on 1/7/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lblVideoName;
@property (weak, nonatomic) IBOutlet UILabel *lblChannel;
@property (weak, nonatomic) IBOutlet UILabel *lblNumberOfViews;
@property (weak, nonatomic) IBOutlet UILabel *lblDuration;

@end

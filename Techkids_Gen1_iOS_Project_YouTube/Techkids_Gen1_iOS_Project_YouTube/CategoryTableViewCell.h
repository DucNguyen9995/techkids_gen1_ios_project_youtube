//
//  CategoriesTableViewCell.h
//  Techkids_Gen1_iOS_Project_YouTube
//
//  Created by Duc Nguyen on 1/15/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lblCategoryName;

@end

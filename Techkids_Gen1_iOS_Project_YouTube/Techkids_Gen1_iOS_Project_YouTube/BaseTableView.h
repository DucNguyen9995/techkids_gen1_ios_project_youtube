//
//  ReuseTableView.h
//  YoutubeParser
//
//  Created by Duc Nguyen on 1/10/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTableView : UITableView

@property(nonatomic, readonly) GTLServiceYouTube *youTubeService;
@property(nonatomic, strong) NSArray *list;
@property(nonatomic, strong) NSMutableDictionary *dictCategoriesDetails;
@property(nonatomic, strong) NSArray *listAlternate;

@end

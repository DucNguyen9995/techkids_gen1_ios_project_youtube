//
//  BaseViewController.h
//  Techkids_Gen1_iOS_Project_YouTube
//
//  Created by Duc Nguyen on 1/25/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController
@property (weak, nonatomic) IBOutlet BaseTableView *tableView;

@end

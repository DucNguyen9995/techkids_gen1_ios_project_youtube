//
//  ViewController.h
//  Techkids_Gen1_iOS_Project_YouTube
//
//  Created by Duc Nguyen on 1/14/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FeaturedViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) IBOutlet BaseTableView *tableView;

@end
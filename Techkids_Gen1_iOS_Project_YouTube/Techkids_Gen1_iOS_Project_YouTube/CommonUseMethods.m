//
//  CommonUseMethods.m
//  Techkids_Gen1_iOS_Project_YouTube
//
//  Created by Duc Nguyen on 1/14/16.
//  Copyright © 2016 Duc Nguyen. All rights reserved.
//

#import "CommonUseMethods.h"

@interface CommonUseMethods ()


@end

@implementation CommonUseMethods

+ (NSString*) parseDuration:(NSString*) duration
{
    //    NSString*resultOfDuration;
    NSInteger hours = 0;
    NSInteger minutes = 0;
    NSInteger seconds = 0;
    
    NSRange timeRange = [duration rangeOfString:@"T"];
    duration = [duration substringFromIndex:timeRange.location];
    
    while (duration.length > 1) {
        duration = [duration substringFromIndex:1];
        
        NSScanner *scanner = [NSScanner.alloc initWithString:duration];
        NSString *part = [NSString.alloc init];
        [scanner scanCharactersFromSet:[NSCharacterSet decimalDigitCharacterSet] intoString:&part];
        
        NSRange partRange = [duration rangeOfString:part];
        
        duration = [duration substringFromIndex:partRange.location + partRange.length];
        
        NSString *timeUnit = [duration substringToIndex:1];
        if ([timeUnit isEqualToString:@"H"])
            hours = [part integerValue];
        else if ([timeUnit isEqualToString:@"M"])
            minutes = [part integerValue];
        else if ([timeUnit isEqualToString:@"S"])
            seconds = [part integerValue];
    }
    if (hours == 0)
    {
        return [NSString stringWithFormat:@"%ld:%02ld", (long)minutes, (long)seconds];
    } else return [NSString stringWithFormat:@"%ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
    
}

+ (NSString*) suffixNumber:(NSNumber*)number
{
    if (!number)
        return @"";
    
    long long num = [number longLongValue];
    
    int s = ( (num < 0) ? -1 : (num > 0) ? 1 : 0 );
    NSString* sign = (s == -1 ? @"-" : @"" );
    
    num = llabs(num);
    
    if (num < 1000)
        return [NSString stringWithFormat:@"%@%lld",sign,num];
    
    int exp = (int) (log(num) / log(1000));
    
    NSArray* units = @[@"K",@"M",@"B",@"T",@"P",@"E"];
    
    return [NSString stringWithFormat:@"%@%.1f%@%@",sign, (num / pow(1000, exp)), [units objectAtIndex:(exp-1)], @" views"];
}

+ (NSString*)formatNumberOfView:(NSString*)number
{
    
    NSNumber*numberOfView = [NSNumber numberWithInteger:[number integerValue]];
    
    
    
    //    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    //    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    //    NSString *result = [formatter stringFromNumber:[NSNumber numberWithInt:numberOfView]];
    NSString *result = [self suffixNumber:numberOfView];
    
    return result;
}


@end
